#################################
# create package
#################################


# check package:
#system("R CMD check rivernet")

# build source package (.tar.gz):
system("R CMD build rivernet")

# check processed package:
system("R CMD check --as-cran rivernet_1.2.3.tar.gz")

install.packages("rivernet_1.2.3.tar.gz",repos=NULL,type="source")
library(rivernet)
help(rivernet)

# upload:
# https://cran.r-project.org/submit.html

